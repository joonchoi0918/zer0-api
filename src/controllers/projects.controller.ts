import { Request, Response } from 'express';
import { Controller, Post, Middleware, Get } from '@overnightjs/core';
import { Project } from '../models';
import {
  validateProjectCreationMiddleware,
  authCheckMiddleware,
  authRequiredMiddleware,
} from '../middlewares';
import { ProjectsService } from '../services';
import { RequestWithUser } from '../utils/types';

@Controller('api/projects')
export class ProjectsController {
  @Post()
  @Middleware(validateProjectCreationMiddleware)
  public async createProject(req: Request, res: Response): Promise<Response> {
    const { name, visibility, status, type, members } = req.body;

    try {
      const project: Project = await ProjectsService.createProject({
        name,
        visibility,
        status,
        type,
        members,
      });

      return res.status(201).send({
        project: project.toDict(),
        message: 'Project created successfully',
      });
    } catch (error) {
      return res.status(error.status).send(error.message);
    }
  }

  @Get('search')
  @Middleware(authCheckMiddleware)
  public async searchProjects(
    req: RequestWithUser,
    res: Response,
  ): Promise<Response> {
    const {
      user,
      query: { member_id: memberId, type, status },
    } = req;

    try {
      const projects: Project[] = await ProjectsService.searchProjects({
        user,
        memberId,
        type,
        status,
      });

      const data = projects.map((project) => project.toDict(user));

      return res.status(200).send({ data });
    } catch (error) {
      return res.status(error.status).send(error.message);
    }
  }

  @Get('myprojects')
  @Middleware(authRequiredMiddleware)
  public async getUserOwnProjects(
    req: RequestWithUser,
    res: Response,
  ): Promise<Response> {
    const { user } = req;

    try {
      const projects: Project[] = await ProjectsService.getUserOwnProjects(
        user,
      );

      const data = projects.map((project) => project.toDict(user));

      return res.status(200).send({ data });
    } catch (error) {
      return res.status(error.status).send(error.message);
    }
  }
}
