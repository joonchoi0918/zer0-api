import { Request, Response } from 'express';
import { Controller, Post, Middleware } from '@overnightjs/core';
import { User } from '../models';
import { signupMiddleware, signinMiddleware } from '../middlewares';
import { UsersService } from '../services';

@Controller('api/user')
export class UsersController {
  @Post('signup')
  @Middleware(signupMiddleware)
  public async signup(req: Request, res: Response): Promise<Response> {
    const { email, name, password } = req.body;

    try {
      const user: User = await UsersService.createUser({
        email,
        name,
        password,
      });

      return res
        .status(201)
        .send({ user: user.toDict(), message: 'Signup successfully' });
    } catch (error) {
      return res.status(error.status).send({ error: error.message });
    }
  }

  @Post('login')
  @Middleware(signinMiddleware)
  public async signin(req: Request, res: Response): Promise<Response> {
    const { email, password } = req.body;

    try {
      const { token, user } = await UsersService.loginUser({
        email,
        password,
      });

      return res
        .status(200)
        .send({ user: user.toDict(), token, message: 'Signin successfully' });
    } catch (error) {
      return res.status(error.status).send({ error: error.message });
    }
  }
}
