import { Request, Response, NextFunction } from 'express';
import { User } from '../models';
import {
  PROJECT_VISIBILITY,
  PROJECT_STATUS,
  PROJECT_TYPE,
} from '../utils/constants';

export const validateProjectCreationMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const { name, visibility, status, type, members = [] } = req.body;

  if (!name) {
    return res.status(400).send({ error: 'Project name is required' });
  }

  if (!visibility) {
    return res.status(400).send({ error: 'Project visibility is required' });
  }

  if (!Object.values(PROJECT_VISIBILITY).includes(visibility)) {
    return res.status(400).send({
      error: `Project visibility should be one of '${Object.values(
        PROJECT_VISIBILITY,
      ).join(' | ')}'`,
    });
  }

  if (!status) {
    return res.status(400).send({ error: 'Project status is required' });
  }

  if (!Object.values(PROJECT_STATUS).includes(status)) {
    return res.status(400).send({
      error: `Project status should be one of '${Object.values(
        PROJECT_STATUS,
      ).join(' | ')}'`,
    });
  }

  if (!type) {
    return res.status(400).send({ error: 'Project type is required' });
  }

  if (!Object.values(PROJECT_TYPE).includes(type)) {
    return res.status(400).send({
      error: `Project type should be one of '${Object.values(PROJECT_TYPE).join(
        ' | ',
      )}'`,
    });
  }

  if (members.length) {
    const { count } = await User.findAndCountAll({ where: { id: members } });

    if (count !== members.length) {
      return res.status(400).send({ error: 'Invalid member ids are provided' });
    }
  }

  next();
};
