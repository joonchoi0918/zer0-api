import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { User } from '../models';
import config from '../config';
import { RequestWithUser } from '../utils/types';

export const signupMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const { email, name, password } = req.body;

  if (!email) {
    return res.status(400).send({ error: 'Email is required' });
  }

  if (!name) {
    return res.status(400).send({ error: 'Username is required' });
  }

  if (!password) {
    return res.status(400).send({ error: 'password is required' });
  }

  next();
};

export const signinMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const { email, password } = req.body;

  if (!email) {
    return res.status(400).send({ error: 'Email is required' });
  }

  if (!password) {
    return res.status(400).send({ error: 'password is required' });
  }

  next();
};

export const authCheckMiddleware = async (
  req: RequestWithUser,
  res: Response,
  next: NextFunction,
) => {
  const headers = req.headers;

  if (headers && headers.authorization) {
    const { id: userId } = await jwt.verify(
      headers.authorization as string,
      config.JWT_SECRET,
    );
    const findUser = await User.findByPk(userId);

    if (findUser) {
      req.user = findUser;
    }
  }

  next();
};

export const authRequiredMiddleware = async (
  req: RequestWithUser,
  res: Response,
  next: NextFunction,
) => {
  try {
    const headers = req.headers;

    if (headers && headers.authorization) {
      const { id: userId } = await jwt.verify(
        headers.authorization as string,
        config.JWT_SECRET,
      );
      const findUser = await User.findByPk(userId);

      if (findUser) {
        req.user = findUser;
        next();
      } else {
        return res.status(401).send({ error: 'Wrong authentication token' });
      }
    } else {
      return res.status(404).send({ error: 'Authentication token is missing' });
    }
  } catch (error) {
    return res.status(401).send({ error: error.message });
  }
};
