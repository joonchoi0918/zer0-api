export class HttpException extends Error {
  public status: number;
  public message: string;
  public isPublic: boolean;

  constructor(status: number, message: string, isPublic = true) {
    super(message);

    this.status = status;
    this.message = message;
    this.isPublic = isPublic;

    Error.captureStackTrace(this, this.constructor);
  }
}
