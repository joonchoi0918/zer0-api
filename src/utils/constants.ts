export enum PROJECT_VISIBILITY {
  PUBLIC = 'Public',
  PRIVATE = 'Private',
}

export enum PROJECT_STATUS {
  LAUNCHING = 'Launching',
  IN_PROGRESS = 'In progress',
  FINISHED = 'Finished',
}

export enum PROJECT_TYPE {
  SHORT_TERM_CONTRACT = 'Short Term Contract',
  LONG_TERM_CONTRACT = 'Long Term Contract',
  OPEN_SOURCE = 'Open Source',
}
