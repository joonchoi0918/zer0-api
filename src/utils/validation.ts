import { PROJECT_VISIBILITY, PROJECT_STATUS, PROJECT_TYPE } from './constants';

export const isValidProjectVisibility = (visibility: string): boolean => {
  return Object.values(PROJECT_VISIBILITY).includes(
    visibility as PROJECT_VISIBILITY,
  );
};

export const isValidProjectStatus = (status: string): boolean => {
  return Object.values(PROJECT_STATUS).includes(status as PROJECT_STATUS);
};

export const isValidProjectType = (type: string): boolean => {
  return Object.values(PROJECT_TYPE).includes(type as PROJECT_TYPE);
};
