import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

export default {
  PORT: process.env.SERVER_PORT || 5000,
  DB_SQLITE_STORAGE: __dirname + './../db/' + process.env.DB_SQLITE_STORAGE,
  JWT_SECRET: process.env.JWT_SECRET,
};
