import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { HttpException } from '../utils/HttpException';
import { User } from '../models';
import config from '../config';

export class UsersService {
  public static async createUser({ email, name, password }): Promise<User> {
    const user: User = await User.findOne({ where: { email } });
    if (user)
      throw new HttpException(409, `You're email ${email} already exists`);

    const hashedPassword = await bcrypt.hash(password, 10);
    const createdUser: User = await User.create({
      email,
      name,
      password: hashedPassword,
    });

    return createdUser;
  }

  public static async loginUser({
    email,
    password,
  }): Promise<{ token: string; user: User }> {
    const user: User = await User.findOne({ where: { email } });

    if (!user) throw new HttpException(409, `Your email ${email} not found`);

    const isPasswordMatching: boolean = await bcrypt.compare(
      password,
      user.password,
    );
    if (!isPasswordMatching)
      throw new HttpException(409, 'Your password is incorrect');

    const token = this.createToken(user);

    return { token, user };
  }

  private static createToken(user: User): string {
    const { id } = user;
    const expiresIn: number = 60 * 60;

    return jwt.sign({ id }, config.JWT_SECRET, { expiresIn });
  }
}
