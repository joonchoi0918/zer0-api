import { Op } from 'sequelize';
import { HttpException } from '../utils/HttpException';
import { Project, User, UserProjectAssociation } from '../models';
import { PROJECT_VISIBILITY } from '../utils/constants';

export class ProjectsService {
  public static async createProject({
    name,
    visibility,
    status,
    type,
    members = [],
  }): Promise<Project> {
    const project: Project = await Project.findOne({ where: { name } });

    if (project)
      throw new HttpException(409, `Project name ${name} already exists`);

    const createdProject: Project = await Project.create({
      name,
      visibility,
      status,
      type,
    });

    if (members.length) {
      await UserProjectAssociation.bulkCreate(
        members.map((userId) => ({
          userId,
          projectId: createdProject.id,
        })),
      );
    }

    const projectInstance = await Project.findByPk(createdProject.id, {
      include: { model: UserProjectAssociation, include: [User] },
    });

    return projectInstance;
  }

  public static async getProjectById(id): Promise<Project> {
    const project: Project = await Project.findByPk(id, {
      include: { model: UserProjectAssociation, include: [User] },
    });

    if (!project)
      throw new HttpException(400, 'Invalid project id is provided');

    return project;
  }

  public static async searchProjects({
    user,
    memberId,
    type,
    status,
  }): Promise<Project[]> {
    const where = {};

    if (user) {
      where['visibility'] = user
        ? { [Op.or]: [PROJECT_VISIBILITY.PUBLIC, PROJECT_VISIBILITY.PRIVATE] }
        : PROJECT_VISIBILITY.PUBLIC;
    }

    if (type) {
      where['type'] = type;
    }

    if (status) {
      where['status'] = status;
    }

    if (memberId) {
      const projectIds = await UserProjectAssociation.findAll({
        where: {
          userId: memberId,
        },
        attributes: ['projectId'],
      }).then((result) => result.map((project) => project.projectId));

      where['id'] = {
        [Op.in]: projectIds,
      };
    }

    const projects: Project[] = await Project.findAll({
      where,
      include: {
        model: UserProjectAssociation,
        include: [User],
      },
    });

    return projects;
  }

  public static async getUserOwnProjects(user: User): Promise<Project[]> {
    const projectIds = await UserProjectAssociation.findAll({
      where: {
        userId: user?.id,
      },
      attributes: ['projectId'],
    }).then((result) => result.map((project) => project.projectId));

    const projects: Project[] = await Project.findAll({
      where: {
        id: {
          [Op.in]: projectIds,
        },
      },
      include: {
        model: UserProjectAssociation,
        include: [User],
      },
    });

    return projects;
  }
}
