import {
  Table,
  Column,
  DataType,
  BelongsTo,
  ForeignKey,
  Model,
} from 'sequelize-typescript';
import { User, Project } from '.';

@Table({
  tableName: 'user_project_association',
})
export class UserProjectAssociation extends Model {
  @BelongsTo(() => User)
  public user!: User;

  @ForeignKey(() => User)
  @Column({ type: DataType.INTEGER })
  public userId!: number;

  @BelongsTo(() => Project)
  public project!: Project;

  @ForeignKey(() => Project)
  @Column({ type: DataType.INTEGER })
  public projectId!: number;
}
