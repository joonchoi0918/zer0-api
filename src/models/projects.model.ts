import {
  Table,
  Column,
  DataType,
  HasMany,
  Model,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';
import { User, UserProjectAssociation } from '.';
import {
  PROJECT_VISIBILITY,
  PROJECT_STATUS,
  PROJECT_TYPE,
} from '../utils/constants';

@Table({
  tableName: 'projects',
})
export class Project extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column({ type: DataType.INTEGER })
  public id!: number;

  @Column({ type: DataType.STRING })
  public name!: string;

  @Column({
    type: DataType.ENUM({ values: Object.values(PROJECT_VISIBILITY) }),
  })
  public visibility!: PROJECT_VISIBILITY;

  @Column({ type: DataType.ENUM({ values: Object.values(PROJECT_STATUS) }) })
  public status!: PROJECT_STATUS;

  @Column({ type: DataType.ENUM({ values: Object.values(PROJECT_TYPE) }) })
  public type!: PROJECT_TYPE;

  @HasMany(() => UserProjectAssociation)
  public userProjectAssociation?: UserProjectAssociation[];

  public toDict(user?: User) {
    const isPrivate = this.visibility === PROJECT_VISIBILITY.PRIVATE;
    const isMyPrivateProject = this.userProjectAssociation?.some(
      (assosication) => assosication.user.id === user?.id,
    );
    const isRestricted = isPrivate && !isMyPrivateProject;

    const members = isRestricted
      ? undefined
      : this.userProjectAssociation?.map((association) =>
          association.user.toDict(),
        );

    return {
      id: this.id,
      name: isRestricted ? undefined : this.name,
      visibility: this.visibility,
      status: this.status,
      type: isRestricted ? undefined : this.type,
      members,
    };
  }
}
