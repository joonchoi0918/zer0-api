import {
  Table,
  Column,
  DataType,
  IsEmail,
  HasMany,
  Model,
  PrimaryKey,
  AutoIncrement,
} from 'sequelize-typescript';
import { UserProjectAssociation } from './';

@Table({
  tableName: 'users',
})
export class User extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column({ type: DataType.INTEGER })
  public id!: number;

  @Column({ type: DataType.STRING })
  public name!: string;

  @IsEmail
  @Column({ type: DataType.STRING })
  public email!: string;

  @Column({ type: DataType.STRING })
  public password!: string;

  @HasMany(() => UserProjectAssociation)
  public userProjectAssociation?: UserProjectAssociation[];

  public toDict() {
    return {
      id: this.id,
      email: this.email,
      name: this.name,
    };
  }
}
