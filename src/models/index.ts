import { Sequelize } from 'sequelize-typescript';
import { User } from './users.model';
import { Project } from './projects.model';
import { UserProjectAssociation } from './user_project_associations.model';
import config from '../config';
import { logger } from '../utils/logger';

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: config.DB_SQLITE_STORAGE,
  logging: (query, time) => {
    logger.info(time + 'ms' + ' ' + query);
  },
  benchmark: true,
});

sequelize
  .authenticate()
  .then(() => {
    logger.info('✅ The database is connected.');
  })
  .catch((error: Error) => {
    logger.error(`🔴 Unable to connect to the database: ${error}.`);
  });

sequelize.addModels([User, Project, UserProjectAssociation]);

export const initDB = async () => {
  await sequelize.authenticate();
  await sequelize.sync({ force: false });
};

export { User, Project, UserProjectAssociation };
