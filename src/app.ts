import { Server } from '@overnightjs/core';
import * as bodyParser from 'body-parser';
import { initDB } from './models';
import { logger } from './utils/logger';
import config from './config';
import { UsersController, ProjectsController } from './controllers';

export class App extends Server {
  constructor() {
    super();

    this.applyMiddleWares();
    this.boostrap();
  }

  public start(): void {
    this.app.listen(config.PORT, () => {
      logger.info(`🚀 App listening on the port ${config.PORT}`);
    });
  }

  private applyMiddleWares() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
  }

  private async boostrap() {
    // Connect to db
    await initDB();

    // add Controllers
    this.addControllers([new UsersController(), new ProjectsController()]);
  }
}
