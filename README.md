## How to run

```bash
yarn install

yarn dev
```

## APIs

```bash


1. POST /api/user/signup

- Request Body 
   {
   	 "email": "example@email.com",
	 "name": "Jone Dae",
	 "password": "P@ssw0rd"
   }
   
- Response
   {
    "user": {
        "id": 1,
        "email": "example@email.com",
	 	"name": "Jone Dae",
    },
    "message": "Signup successfully"
   }
   
   
2. POST /api/user/login

- Request Body 
   {
   	 "email": "example@email.com",
	 "password": "P@ssw0rd"
   }
   
- Response
   {
    "user": {
        "id": 1,
        "email": "example@email.com",
	 	"name": "Jone Dae",
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiaWF0IjoxNjQ2NzQ3MTIwLCJleHAiOjE2NDY3NTA3MjB9.XSxtgKccsbEfaJ1nuWqhwRJo84s9MhkL8P1WExHHE6k",
    "message": "Signin successfully"
   }
   
   
 3. POST /api/projects
 
 - Request Body
 	{
		"name": "Project Name",
		"visibility": "Public", // "Public" or "Private"
		"status": "Finished", // "Launching" or "In progress" or "Finished"
		"type": "Short Term Contract", // "Short Term Contract" or "Long Term Contract" or "Open Source" 
		"members": [2, 3, 4]
	}
	
- Response
	{
		"project": {
			"id": 1,
			"name": "Project Name",
			"visibility": "Public",
			"status": "Finished",
			"type": "Short Term Contract",
			"members": [
				{
					"id": 2,
					"email": "joon.choi0919@gmail.com",
					"name": "Joon"
				},
				{
					"id": 3,
					"email": "joon.choi0920@gmail.com",
					"name": "Joon"
				},
				{
					"id": 4,
					"email": "joon.choi0921@gmail.com",
					"name": "Joon"
				}
			]
		},
		"message": "Project created successfully"
	}
	
	
4. GET /api/projects/search

- Request Header (Optional JWT token and it will be retreived from /login API)
	Authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiaWF0IjoxNjQ2NzQ3MTIwLCJleHAiOjE2NDY3NTA3MjB9.XSxtgKccsbEfaJ1nuWqhwRJo84s9MhkL8P1WExHHE6k"

- Request Params
	"status": "Launching" or "In progress" or "Finished"
	"type": "Short Term Contract" or "Long Term Contract" or "Open Source"
	"member_id": User id to search
	
	Example URL: /api/projects/search?status=Finished&type=Open Source&member_id=100

- Response
	{
		"data": [
			{
				"id": 31,
				"visibility": "Private",
				"status": "Finished",
			}, // Hide the "name", "type", "members" for the "Private" project which is not contains authorized user as a member, anonymous users will see only "Public" projects
			{
				"id": 32,
				"name": "Project 32",
				"visibility": "Public",
				"status": "Finished",
				"type": "Short Term Contract",
				"members": [
					{
						"id": 2,
						"email": "joon.choi0919@gmail.com",
						"name": "Joon"
					},
					{
						"id": 3,
						"email": "joon.choi0920@gmail.com",
						"name": "Joon"
					},
					{
						"id": 4,
						"email": "joon.choi0921@gmail.com",
						"name": "Joon"
					}
				]
			}
		]
	}
	
	
5. GET /api/projects/myprojects
	
- Request Header (Required JWT token and it will be retreived from /login API)
	Authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiaWF0IjoxNjQ2NzQ3MTIwLCJleHAiOjE2NDY3NTA3MjB9.XSxtgKccsbEfaJ1nuWqhwRJo84s9MhkL8P1WExHHE6k"

- Response
	{
		"data": [
			{
				"id": 12,
				"name": "Project 12",
				"visibility": "Public",
				"status": "Lunching",
				"type": "Short Term Contract1",
				"members": [
					{
						"id": 2,
						"email": "joon.choi0919@gmail.com",
						"name": "Joon"
					},
					{
						"id": 3,
						"email": "joon.choi0920@gmail.com",
						"name": "Joon"
					}
				]
			},
			{
				"id": 13,
				"name": "Project 13",
				"visibility": "Public",
				"status": "Lunching",
				"type": "Short Term Contract1",
				"members": [
					{
						"id": 2,
						"email": "joon.choi0919@gmail.com",
						"name": "Joon"
					},
					{
						"id": 3,
						"email": "joon.choi0920@gmail.com",
						"name": "Joon"
					}
				]
			},
			....
		]
	}
```
